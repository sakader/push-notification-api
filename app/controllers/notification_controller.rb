class NotificationController < ApplicationController
  def dashboard

  end
  def notify
        #get all devices registered in our db and loop through each of them
    Device.all.each do |device|
      n = Rpush::Gcm::Notification.new
            # use the pushme_droid app we previously registered in our initializer file to send the notification
      n.app = Rpush::Gcm::App.find_by_name("pushme")
      n.registration_ids = [device.token]

            # parameter for the notification
      n.notification = {
        body: 'This is the notification body . . .!',
           	title: 'Hey this is notification title!',
           	sound: 'default'
       	}
            #save notification entry in the db
      n.save!
    end

        # send all notifications stored in db
        Rpush.push

    render json: {sent: true}, status: :ok
  end
end
