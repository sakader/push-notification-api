Rails.application.routes.draw do
  resources :devices
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get '/notification/notify' => 'notification#notify'

  resources :notification do
    get :dashboard
  end

  root 'notification#dashboard'
end
